package project2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class project2 {

	// Connection credentials for the database
	static String db = "jdbc:postgresql://cmsc461db.c397cbijraeq.us-west-2.rds.amazonaws.com:5432/devdb";
	static String username = "umbc";
	static String password = "Brykim91";
	
	public static void main(String[] args) {
		FileReader file = null;
		try {
			file = new FileReader("config.txt");
		} 
		catch (Exception e) 
		{
			System.out.print("Could not find file");
			System.exit(1);
		}
		
		try {
		//Read in database information from the config file
		BufferedReader reader = new BufferedReader(file);
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			if(line.equalsIgnoreCase("#Database")) {
				db = reader.readLine();
				System.out.println(db);
			}
			
			else if(line.equalsIgnoreCase("#Username")) {
				username = reader.readLine();
				System.out.println(username);
			}
			
			else if(line.equalsIgnoreCase("#Password")) {
				password = reader.readLine();
				System.out.println(password);
			}
			
			else {
			}
		}
		}
		catch (Exception e)
		{
			System.out.print(e);
			System.exit(1);
		}
		Connection conn = null;
		Statement stmt = null;

		try {
			// Configure driver for database connection
			Class.forName("org.postgresql.Driver");

			// Connect to the database
			System.out.print("Connecting to the db \n");
			Properties property = new Properties();
			property.setProperty("user", username);
			property.setProperty("password", password);
			conn = DriverManager.getConnection(db, property);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Opened database successfully");

		try {
			// Create a table in the database
			System.out.println("Creating a table in the database");
			stmt = conn.createStatement();
			String sql = "CREATE TABLE EMPLOYEE "
					+ "(ID INT PRIMARY KEY       NOT NULL,"
					+ " NAME           TEXT      NOT NULL,"
					+ " AGE            INT       NOT NULL,"
					+ " POSITION       CHAR(20), " + " RATE           REAL)";
			stmt.execute(sql);
			System.out.println("Table created successfully");

			// Insert rows into table
			System.out.println("Inserting 20 values into table");

			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (1, 'Allen', 22, 'Programmer', 20.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (2, 'Dan', 24, 'Programmer', 21.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (3, 'Ben', 25, 'Programmer', 21.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (4, 'Ruth', 27, 'Manager', 40.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (5, 'Stacy', 28, 'Manager', 40.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (6, 'Rob', 25, 'Analyst', 20.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (7, 'Joe', 21, 'Programmer', 30.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (8, 'Jill', 29, 'Analyst', 29.50 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (9, 'Kia', 24, 'Analyst', 31.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (10, 'Rachael', 22, 'Sales', 20.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (11, 'Mike', 27, 'Sales', 21.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (12, 'Winston', 30, 'HR', 30.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (13, 'Joanne', 19, 'CEO', 80.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (14, 'Nico', 27, 'HR', 25.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (15, 'Trish', 22, 'Sales', 23.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (16, 'Charles', 31, 'Programmer', 35.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (17, 'Mable', 28, 'HR', 30.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (18, 'Alex', 24, 'Sales', 25.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (19, 'Yandy', 32, 'Analyst', 40.00 );";
			stmt.execute(sql);
			sql = "INSERT INTO EMPLOYEE (ID,NAME,AGE,POSITION,RATE) "
					+ "VALUES (20, 'Paul', 29, 'Programmer', 28.00 );";
			stmt.execute(sql);

			System.out.println("Records created");

			System.out.println("Selecting every value from the table");
			ResultSet result = stmt.executeQuery("SELECT * FROM EMPLOYEE;");
			while (result.next()) {
				int id = result.getInt("id");
				String name = result.getString("name");
				int age = result.getInt("age");
				String position = result.getString("position");
				float rate = result.getFloat("rate");
				System.out.println("ID: " + id);
				System.out.println("Name: " + name);
				System.out.println("Age: " + age);
				System.out.println("Position: " + position);
				System.out.println("Rate: " + rate + "\n");
			}

			System.out.println("Selecting all employees over the age of 30");
			ResultSet secondResult = stmt
					.executeQuery("SELECT * FROM EMPLOYEE WHERE AGE >= 30");
			while (secondResult.next()) {
				int id = secondResult.getInt("id");
				String name = secondResult.getString("name");
				int age = secondResult.getInt("age");
				String position = secondResult.getString("position");
				float rate = secondResult.getFloat("rate");
				System.out.println("ID: " + id);
				System.out.println("Name: " + name);
				System.out.println("Age: " + age);
				System.out.println("Position: " + position);
				System.out.println("Rate: " + rate + "\n");
			}

			System.out.println("Updating pay rates of 3 employees");
			sql = "UPDATE EMPLOYEE set RATE = 38.00 where ID=1;";
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set RATE = 42.00 where ID=7;";
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set RATE = 50.00 where ID=11;";
			stmt.executeUpdate(sql);
			
			System.out.println("Updating the position of 2 employees");
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set POSITION = 'Analyst' where ID=1;";
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set POSITION = 'IS' where ID=2;";
			
			System.out.println("Updating the age of 2 employees");
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set AGE = 29 where ID=5;";
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set AGE = 26 where ID=6;";
			stmt.executeUpdate(sql);
			sql = "UPDATE EMPLOYEE set AGE = 22 where ID=7;";
			
			System.out.println("Printing out updated row");
			ResultSet thirdResult = 
					stmt.executeQuery("SELECT * FROM EMPLOYEE WHERE ID = 1 OR ID = 2 OR ID= 5 OR ID = 6 OR ID = 7 OR ID = 11");
			while (thirdResult.next()) {
				int id = thirdResult.getInt("id");
				String name = thirdResult.getString("name");
				int age = thirdResult.getInt("age");
				String position = thirdResult.getString("position");
				float rate = thirdResult.getFloat("rate");
				System.out.println("ID: " + id);
				System.out.println("Name: " + name);
				System.out.println("Age: " + age);
				System.out.println("Position: " + position);
				System.out.println("Rate: " + rate + "\n");
			}
			System.out.println("Deleting all rows from the table");
			sql = "DELETE FROM EMPLOYEE;";
	        stmt.executeUpdate(sql);
	        
			stmt.close();
			conn.close();

		} catch (SQLException e) {
			System.out.print("Table already exists in the database");
			System.exit(0);
		}
	}

}
